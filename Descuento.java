package org.pablo.beans;
import java.util.ArrayList;
public class Descuento{

	private int  porcentaje;
	private int total;
	private int ahorro;
	private String id;
	public Descuento(){}
	private ArrayList<Descuento> propiedadDescuento;
	
	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id=id;
	}
	public int getPorcentaje(){
		return this.porcentaje;
	}
	public void setPorcentaje(int porcentaje){
		this.porcentaje=porcentaje;
	}
	public int getTotal(){
		return this.total;
	}
	public void setTotal(int total){
		this.total=total;
	}
	public void setAhorro(int ahorro){
		this.ahorro=ahorro;
	}
	public int getAhorro(){
		return this.ahorro;
	}
	public ArrayList<Descuento> getListaDescuento(){
		return this.propiedadDescuento;
	}
	public void setListaDescuento(ArrayList<Descuento>propiedadDescuento){
		this.propiedadDescuento=propiedadDescuento;
	}
	public Descuento(String id,int porcentaje,int total,int ahorro){
		this.setId(id);
		this.setPorcentaje(porcentaje);
		this.setTotal(total);
		this.setAhorro(ahorro);
	}
}