/**
* Interfaz 
* @author Pablo Hernandez
*/

package org.pablo.utilidades.eventos;

import java.util.HashMap;
import java.util.ArrayList;

public interface DecodeListener{

	void avisarAccionar(String accion, HashMap<String, String> parametros,ArrayList valorUnico);
}