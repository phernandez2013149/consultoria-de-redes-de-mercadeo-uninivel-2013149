/**
* Aplicacion Sistema login 
* @author Pablo Hernandez
*/
package org.pablo.sistema;

import org.pablo.utilidades.Lee;
import org.pablo.utilidades.Decodificador;
import org.pablo.manejadores.ManejadorUsuario;
import org.pablo.manejadores.ManejadorMiembro;
import org.pablo.app.AppAdmin;
import org.pablo.app.AppMiembro;
import org.pablo.app.AbstractAppRol;
import org.pablo.utilidades.Complementos;
import org.pablo.utilidades.Borrar;

public class Sistema{
	
	public void iniciar(){
	boolean salir = true;
	Lee leer = new Lee();
	Borrar borrar = new Borrar();
	Complementos complemento = new Complementos();
			borrar.limpiar(2);	
		do{
			System.out.println("	-Login-");	
			AbstractAppRol app = null;		
			
			AbstractAppRol app1 = null;		
			String nick, password;
			borrar.limpiar(5);			
			System.out.print("User>> ");
			nick=leer.dato();
			if(nick.equals("exit")){
				salir=false;
			
			}else{
				System.out.print("Password>>");
				password=leer.dato();
				borrar.limpiar(30);
				boolean resultado = ManejadorUsuario.getInstancia().autenticarUsuario(nick, password);
				boolean resultadoM = ManejadorMiembro.getInstancia().autenticarUsuario(nick, password);

				if(resultado){
					
					switch(ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getRol()){
						case "admin":
							System.out.println("Bienvenido "+ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getNombre());
							app=new AppAdmin(new Decodificador());
							app.iniciar();
							break;
						default:
							break;
					}
				}else if(resultadoM){
				
					switch(ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado().getRol()){
						case "miembro":
						System.out.println("Bienvenido "+ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado().getNombre());
							app1=new AppMiembro(new Decodificador());
							app1.iniciar();
						break;
						default:
						break;
					}
				}

			}	
			borrar.limpiar(2);
					
		}while(salir);
	
}

}
