Nombre:Pablo Esteban Hernandez
id:2013146 IN5BV

	Readme
Funciones:
agregar downlines-creaciones de subredes-calculo de ganancias-
control de ventas- administracion
Mapa:
	mim							admin
	---							------
	add downline parametro=dato [down=upLine]		add user parametro=dato
	show me [money]						show me [money]
	list products						list products
	show product nombre=nombreDelProducto			show product nombre=nombreDelProducto
	show downline nick=nickDelDownline			show downline nick=nickDelDownline
	show history						show history
	show history buy					show history buy
	show history downline nick=nickDownline			show history downline nick=nickDownline
	search dato=AlgunDato					search dato=AlgunDato
	buy product id=idProducto				[add|remove|edit] product id=idProducto
	edit me propiedad=nuevo valor				show sales [id=idProducto]
	buy product id=ofert,idProducto				add ofert idproduct=id cantidad=cantidadDeProductos precioDe=numeroDeUnidadesQueSeComprariaNomalmente
	buy product id=desc,idProducto				add ofert idproduct=id porcentaje=porcentajeDelDescuento
	logout							SELECT campo,campo,campo FROM tabla
								logout