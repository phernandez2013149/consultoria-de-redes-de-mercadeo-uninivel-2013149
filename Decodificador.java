/**
* utilidades Dedificador devuelve un comando decodificaco 
* @author Pablo Hernandez
*/
package org.pablo.utilidades;

import org.pablo.utilidades.eventos.DecodeListener;

import java.util.HashMap;
import java.util.ArrayList;

public class Decodificador{
	
	private ArrayList<String> valorUnico=new ArrayList<String>();
	private DecodeListener decodeListener;	
	public void addDecodeListener(DecodeListener decodeListener){
		this.decodeListener=decodeListener;
	}
	
	public void decodificarComando(String comando){
		
		String[] comandos=comando.split(" ");
		HashMap<String,String> diccionario = new HashMap<String,String>();
		String accion = null;
		valorUnico.clear();
		
		if(comandos[0].equals("select")){
			accion ="select";
			String claveValor[] = comandos[1].split(",");
				for(String posicion:claveValor)
					valorUnico.add(posicion);
			diccionario.put(comandos[2], comandos[3]);
			boolean condicion=false;
			try{
				if(comandos[4].equals("where"));
					condicion=true;
					String [] temp=comandos[5].split("=");
					diccionario.put(temp[0],temp[1]);
			}catch(ArrayIndexOutOfBoundsException error){}
			
		}else{
			
			if(comandos.length>1){
				accion = comandos[0]+" "+comandos[1];
				valorUnico.add(comandos[0]);
			}else if(comandos.length==1){
				accion= comandos[0];
				valorUnico.add(comandos[0]);
			}
			
			for(int posicion=2;posicion<comandos.length;posicion++){
				String claveValor[] = comandos[posicion].split("=");
				valorUnico.add(comandos[posicion]);
				try{
					diccionario.put(claveValor[0], claveValor[1]);
				}catch(ArrayIndexOutOfBoundsException error){}
			
			}
		}
		this.decodeListener.avisarAccionar(accion, diccionario,valorUnico);
	}	
}