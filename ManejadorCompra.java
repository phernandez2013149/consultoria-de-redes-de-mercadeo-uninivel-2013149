/**
* manejadores ManejadorCompra acciones de Compra 
* @author Pablo Hernandez
*/

package org.pablo.manejadores;

import java.util.ArrayList;

import org.pablo.beans.Compra;
import org.pablo.beans.Oferta;

public class ManejadorCompra{
	private ArrayList<Compra> listaCompra;
	private ArrayList<Compra> lista;
	private ArrayList<Oferta> listaOferta;

	private static ManejadorCompra instancia;
	
	private ManejadorCompra(){
		this.listaCompra=new ArrayList<Compra>();
		this.listaOferta=new ArrayList<Oferta>();
	}		
		
	public void agregaCompra(Compra compra){
		this.listaCompra.add(compra);
	}
	public Oferta buscarOferta(String id){
		for(Oferta oferta : this.listaOferta){
			if(oferta.getId().equals(id))
				return oferta;	
		}
		return null;
	}

	public boolean ofertaExistente(String idOferta){			
		Oferta oferta=this.buscarOferta(idOferta);
		if(oferta!=null)
			return true;
		return false;
	}
	public ArrayList<Compra> obtenerListaCompra(){
		return this.listaCompra;
	}
	public ArrayList<Oferta> obtenerListaOferta(){
		return this.listaOferta;
	}
	
	public static ManejadorCompra getInstancia(){
		if(instancia==null)
			instancia=new ManejadorCompra();
		return instancia;
	}
}




