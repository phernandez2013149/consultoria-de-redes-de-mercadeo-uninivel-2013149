/**
* Manejadores manejadorUsuario aplicando encapsulamiento 
* @author Pablo Hernandez
*/


package org.pablo.manejadores;

import java.util.ArrayList;
import java.util.Date;

import org.pablo.beans.Usuario;
import org.pablo.beans.Miembro;
import org.pablo.manejadores.ManejadorMiembro;

public class ManejadorUsuario{
	private ArrayList<Usuario> listaUsuario;
	private Usuario usuarioAutenticado;
	private static ManejadorUsuario instancia;

		
	private ManejadorUsuario(){
		this.listaUsuario=new ArrayList<Usuario>();
		Usuario usuario1=new Usuario(); 
		Usuario usuario2=new Usuario(); 
		usuario1.setNombre("nombre");
		usuario1.setApellido("apellido");
		usuario1.setNick("admin");
		usuario1.setPassword("admin");
		usuario1.setRol("admin");
		usuario1.setEdad(18);
		usuario1.setFecha(new Date());
		this.listaUsuario.add(usuario1);
	}
	
	public void agregarUsuario(Usuario usuario){
		this.listaUsuario.add(usuario);
	}
	public void eliminarUsuario(Usuario usuario){
		this.listaUsuario.remove(usuario);
	}
	public ArrayList<Usuario> obtenerListaUsuario(){
		return this.listaUsuario;
	}
	public Usuario buscarUsuario(String nick){
		for(Usuario usuario : this.listaUsuario){
			if(usuario.getNick().equals(nick)){
				return usuario;
			}
		}
		return null;
	}

	public boolean existe(String nick){
		Usuario db = this.buscarUsuario(nick);
		Miembro db1=ManejadorMiembro.getInstancia().buscarUsuario(nick);
		if(db!=null||db1!=null)
			return true;
		return false;
	}
	public int ganancia(int nivel,int compra){
		int formula=0;
		if(nivel==1 || nivel>5&nivel<11){
			formula=(compra*2)/100;
		}else if(nivel==2){
			formula=(compra*25)/100;
		}else if(nivel==3){
			formula=(compra*6)/100;
		}else if(nivel<5&nivel>2){
			formula=(compra*3)/100;
		}
		return formula;
	}
	public boolean autenticarUsuario(String nick, String password){
		Usuario usuarioBuscado = this.buscarUsuario(nick);
		if(usuarioBuscado!=null){
			if(usuarioBuscado.getPassword().equals(password)){
				this.usuarioAutenticado = usuarioBuscado;
				return true;
			}
		}
		return false;
	}
	public void desautenticarUsuario(){
		this.usuarioAutenticado=null;
	}
	public Usuario obtenerUsuarioAutenticado(){
		return this.usuarioAutenticado;
	}
	public static ManejadorUsuario getInstancia(){
		if(instancia==null)
			instancia=new ManejadorUsuario();
		return instancia;
	}
}
