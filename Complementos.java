/**
* utilidad Complementos ayuda al usuario 
* @author Pablo Hernandez
*/


package org.pablo.utilidades;

import org.pablo.sistema.Sistema;

public class Complementos{
/** @param error muestra el error*/
	public void referencia(String error){
		System.out.println( "		'"+error+"' no se reconoce como comando interno o externo");
		System.out.println( "	 	para obtener mas infomacion acerca de un comando ");
		System.out.println( " 		especifico escriba HELP");	
	}
	public void abstrac(){
	
		System.out.println( "-LIST PRODUCTS: lista de productos");	
		System.out.println( "-SHOW PRODUCT   detalles del producto ");	
		System.out.println( "-SHOW DOWNLINE  detalles de x downline");
		System.out.println( "-LIST DOWNLINES  lista de downlies ");	
		System.out.println( "-SHOW ME    informacion de mi perfil");	
		System.out.println( "-SHOW HISTORY muestra la informacio ");			
		System.out.println( "-SEARCH DOWNLINE  realiza una busqueda ");
		System.out.println( "-LOGOUT  cerrar sesion");	
		System.out.println( "-EXIT	cerrar programa");	
	
	}

	public void helpAdmin(){
		this.abstrac();
		System.out.println( "-ADD PRODUCT  agrega producto  ");
		System.out.println( "-REMOVE PRODUCT  elimina un product ");
		System.out.println( "-EDIT PRODUCT  edita un producto");	
		System.out.println( "-SHOW SALES   historial de ventas");
		System.out.println( "-ADD USER agrega un administrador");
		}
	
	public void helpMiembro(){
		this.abstrac();
		System.out.println( "-ADD DOWNLINE	agregar un nuevo downline");
		System.out.println( "-BUY PRODUCT	comprar producto con el id ");
		System.out.println( "-EDIT ME  modifica uno o mas campos de mi perfil");				
	}

}