/**
* manejadores ManejadorMiembro acciones de Miembro 
* @author Pablo Hernandez
*/

package org.pablo.manejadores;

import java.util.ArrayList;
import java.util.Date;

import org.pablo.beans.Usuario;
import org.pablo.beans.Miembro;
import org.pablo.beans.ListaInterna;
import org.pablo.manejadores.ManejadorUsuario;

public class ManejadorMiembro{
	private ArrayList<Miembro> listaMiembro;
	private ArrayList<Miembro> misDownlines;
	private Miembro usuarioAutenticado;
	private static ManejadorMiembro instancia;
	private Date date= new Date();
	
	private ManejadorMiembro(){
		this.listaMiembro=new ArrayList<Miembro>();
		Miembro usuario2=new Miembro(); 
		usuario2.setNombre("Nmiembro");
		usuario2.setApellido("Amiembro");
		usuario2.setNick("mim");
		usuario2.setPassword("mim");
		usuario2.setRol("miembro");
		usuario2.setUpLine("@miembro");
		usuario2.setInscriptor("@miembro");
		usuario2.setCompra(100);
		usuario2.setFecha(date);
		usuario2.setNivel(0);	
		usuario2.setEdad(18);
		usuario2.setPin(00000);
		this.listaMiembro.add(usuario2);
	}

	public void agregarUsuario(Miembro usuario){
		this.listaMiembro.add(usuario);
	}
	public void eliminarUsuario(Miembro usuario){
		this.listaMiembro.remove(usuario);
	}
	public ArrayList<Miembro> obtenerListaMiembro(){
		return this.listaMiembro;
	}
	public Miembro buscarUsuario(String nick){
		for(Miembro usuario : this.listaMiembro){
			if(usuario.getNick().equals(nick)){
				return usuario;
			}
		}
		return null;
	}
	public Miembro buscarUpLine(String upLine){
		
		for(Miembro usuario : this.listaMiembro){
			if(usuario.getNick().equals(upLine)){
				return usuario;
			}
		}
		return null;
	}
	
	public int ganancia(int nivel,int compra){
		int formula=0;
		if(nivel==1 || nivel>5&nivel<11){
			formula=(compra*2)/100;
		}else if(nivel==2){
			formula=(compra*25)/100;
		}else if(nivel==3){
			formula=(compra*6)/100;
		}else if(nivel<5&nivel>2){
			formula=(compra*3)/100;
		}
		return formula;
	}
	public ArrayList<Miembro> listarDownlines(Miembro miembro){
	
		for(Miembro mim : miembro.getListaMiembro()){
			misDownlines.add(mim);
			
			this.listarDownlines(mim);
		}
		return misDownlines;
	}
	public boolean autenticarUsuario(String nick, String password){
		Miembro usuarioBuscado = this.buscarUsuario(nick);
		if(usuarioBuscado!=null){
			if(usuarioBuscado.getPassword().equals(password)){
				this.usuarioAutenticado = usuarioBuscado;
				return true;
			}
		}
		return false;
	}
	public void desautenticarUsuario(){
		this.usuarioAutenticado=null;
	}
	public Miembro obtenerUsuarioAutenticado(){
		return this.usuarioAutenticado;
	}
	public boolean existe(String nick){
		Miembro db = this.buscarUsuario(nick);
		Usuario bd1=ManejadorUsuario.getInstancia().buscarUsuario(nick);
		if(db!=null||bd1!=null)
			return true;
		return false;
	}

	public Miembro buscarDentroDeMiembro(Miembro miembro,String nick){

		for(Miembro miembroDentro: miembro.getListaMiembro()){
			if(miembroDentro.getNick().equals(nick))
				return miembroDentro;
		}
		return null;
	}
	public static ManejadorMiembro getInstancia(){
		if(instancia==null)
			instancia=new ManejadorMiembro();
		return instancia;
	}
}
