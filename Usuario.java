/**
* bean Usuaio aplicando encapsulamiento 
* @author Pablo Hernandez
*/

package org.pablo.beans;
import java.util.Date;

public class Usuario{
	private String nombre;
	private String apellido;
	private String nick;
	private String password;
	private String rol;
	private int edad;
	private Date fecha;

	
	public String getNombre(){
		return this.nombre;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getApellido(){
		return this.apellido;
	}
	public void setApellido(String apellido){
		this.apellido=apellido;
	}
	public String getNick(){
		return this.nick;
	}
	public void setNick(String nick){
		this.nick=nick;
	}
	public String getPassword(){
		return this.password;
	}
	public void setPassword(String password){
		this.password=password;
	}
	public String getRol(){
		return this.rol;
	}
	public void setRol(String rol){
		this.rol=rol;
	}
	public int getEdad(){
		return this.edad;
	}
	public void setEdad(int edad){
		this.edad=edad;
	}
	public Date getFecha(){
		return this.fecha;
	}
	public void setFecha(Date fecha){
		this.fecha=fecha;
	}
	public Usuario(){

	}
	public Usuario(String nombre,String apellido,String nick,String password,String rol,int edad,Date fecha){
	
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setNick(nick);
		this.setPassword(password);
		this.setRol(rol);
		this.setEdad(edad);
		this.setFecha(fecha);
		

	}
}