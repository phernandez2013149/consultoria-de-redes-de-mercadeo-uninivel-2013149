/**
* bean compra aplicando encapsulamiento 
* @author Pablo Hernandez
*/

package org.pablo.beans;

import java.util.ArrayList;
import java.util.Date;
public class Compra{
	private String miembro;
	private String producto ;
	private Date date ;
	private ArrayList<Compra> listaCompra;
	
	/** @retur miembro devuelve el nombre del Miembro*/
	public String getMiembro(){
		return this.miembro;
	}
	/** @param miembro pedimos un String para almacenarlo*/
	public void setMiembro(String miembro){
		this.miembro=miembro;
	}
	/** @retur producto devuelve el Stirng producto*/
	public String getProducto(){
		return this.producto;
	}
	/**@param producto pedimos un String para almacenarlo*/
	public void setProducto(String producto){
		this.producto=producto;
	}
	/** @retur date devuelve la fecha*/
	public Date getDate(){
		return this.date;
	}
	/** @param date pedimos la fecha para almacenarla*/
	public void setDate(Date date){
		this.date=date;
	}
	/** @retur listaCompra una lista con todas las compras*/
	public ArrayList<Compra> getListaCompra(){
		return this.listaCompra;
	}
	/** @param listaCompra pedimos una lista para almacenarl*/	
	public void setListaCompra(ArrayList<Compra>listaCompra){
		this.listaCompra=listaCompra;
	}
	public Compra(){
	}

	public Compra(String miembro, int pin,Date date,String producto){
		this.setMiembro(miembro);
		this.setDate(date);
		this.setProducto(producto);
	}

}

	