/**
* Sistema App clasePrincipal
* @author Pablo Hernandez
*/

package org.pablo.sistema;
public class App{
	public static void main(String args[]){
		new Sistema().iniciar();
	}
}
