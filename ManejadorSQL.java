package org.pablo.manejadores;

import java.util.ArrayList;

import org.pablo.manejadores.ManejadorUsuario;
import org.pablo.manejadores.ManejadorProducto;
import org.pablo.manejadores.ManejadorMiembro;
import org.pablo.manejadores.ManejadorCompra;
import org.pablo.beans.Compra;
import org.pablo.beans.Miembro;
import org.pablo.beans.Usuario;
import org.pablo.beans.Producto;

public class ManejadorSQL{

	private static ManejadorSQL instancia;	
	private ArrayList<String>campos;
	private ArrayList<Usuario>listaUsuario;
	private ArrayList<Producto>listaProducto;
	private ArrayList<Miembro>listaMiembro;
	private ArrayList<Compra>listaCompra;	
	
	public ManejadorSQL(){
		this.campos=new ArrayList<String>();
		this.listaUsuario=new ArrayList<Usuario>();
		this.listaProducto=new ArrayList<Producto>();
		this.listaMiembro=new ArrayList<Miembro>();
		this.listaCompra=new ArrayList<Compra>();
	}
	
	public void aQueMeRefiero(String objeto,ArrayList<String> campos){
		this.campos=campos;
		switch(objeto){
		case "downline":
			this.listaMiembro=ManejadorMiembro.getInstancia().obtenerListaMiembro();
			for(String variable:this.campos){
				for(Miembro miembro:this.listaMiembro){
					switch(variable){
						case "nombre":
							System.out.println("--NOMBRE--");
							System.out.print(miembro.getNombre());
							System.out.println("");
						break;
						case "apellido":
							System.out.println("--APELLIDO--");
							System.out.print(miembro.getApellido());
							System.out.println("");
						break;
						case "edad":
							System.out.println("--EDAD--");
							System.out.print(miembro.getEdad());
							System.out.println("");
						break;
					}
				}
			}
		break;
		case "admin":
			this.listaUsuario=ManejadorUsuario.getInstancia().obtenerListaUsuario();
			for(String variable:this.campos){
				for(Usuario usuario:this.listaUsuario){
					switch(variable){
						case "nombre":
							System.out.println("--NOMBRE--");
							System.out.print(usuario.getNombre());
							System.out.println("");
						break;
						case "apellido":
							System.out.println("--APELLIDO--");
							System.out.print(usuario.getApellido());
							System.out.println("");
						break;
						case "edad":
							System.out.println("--EDAD--");
							System.out.print(usuario.getEdad());
							System.out.println("");
						break;
					}
				}
			}
		break;
		case "products":
			this.listaProducto=ManejadorProducto.getInstancia().obtenerListaProducto();
			for(String variable:this.campos){
				for(Producto product:this.listaProducto){
					switch(variable){
						case "nombre":
							System.out.println("--PRODUCTO--");
							System.out.print(product.getNombre());
							System.out.println("");
						break;
						case "precio":
							System.out.println("--PRECIO--");
							System.out.print(product.getPrecio());
							System.out.println("");
						break;
						case "id":
							System.out.println("--ID--");
							System.out.print(product.getId());
							System.out.println("");
						break;
					}
				}
			}
		break;
		
		}
		
	}
	
	public static ManejadorSQL getInstancia(){
		if(instancia==null)
			instancia=new ManejadorSQL();
		return instancia;
	} 
} 