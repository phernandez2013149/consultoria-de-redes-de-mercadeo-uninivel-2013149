/**
* bean Miembro aplicando encapsulamiento 
* @author Pablo Hernandez
*/

package org.pablo.beans;

import java.util.ArrayList;
import java.util.Date;

import org.pablo.beans.Usuario;
import org.pablo.beans.Compra;

public class Miembro extends Usuario {
	private String upLine;
	private int pin;
	private int compra;
	private int nivel;
	private String inscriptor;
	private ArrayList<Miembro> listaMiembro;

	
	public void setInscriptor(String inscriptor){
		this.inscriptor=inscriptor;
	}
	public String getInscriptor(){
		return this.inscriptor;
	}	
	public void setNivel(int nivel){
		this.nivel=nivel;
	}
	public int getNivel(){
		return this.nivel;
	}
	public int getCompra(){
		return this.compra;
	}
	public void setCompra(int compra){
		this.compra=compra;
	}
	public void setListaMiembro(ArrayList<Miembro> lista){
		this.listaMiembro=lista;
	}
	public 	ArrayList<Miembro> getListaMiembro(){
		return this.listaMiembro;
	}
	public String getUpLine(){
		return this.upLine;
	}
	public void setUpLine(String upLine){
		this.upLine=upLine;
	}	
	public int getPin(){
		return this.pin;
	}
	public void setPin(int pin){
		this.pin=pin;
	}
	public Miembro(){
		setListaMiembro(new ArrayList<Miembro>());
	}
	public Miembro(String nombre,String apellido,String nick,String password,String rol,int edad,Date fecha,String upLine,String inscriptor,int pin,int nivel,int compra){
		super(nombre,apellido,nick,password,rol,edad,fecha);
		this.setUpLine(upLine);
		this.setInscriptor(inscriptor);
		this.setPin(pin);
		this.setNivel(nivel);
		this.setCompra(compra);
	}
}