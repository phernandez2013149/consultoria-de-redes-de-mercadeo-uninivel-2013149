/**
* utilidades Lee Leemos un String 
* @author Pablo Hernandez
*/
package org.pablo.utilidades;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Lee{
BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
String Variable;
	/** @retur dato lee un String */
		public String dato(){
			try{
				Variable=br.readLine().toLowerCase();
			
			}catch(IOException ioe){
				
				System.out.println("Error en la captura de datos ");
				System.exit(1);
			}
		return Variable;
		}
}