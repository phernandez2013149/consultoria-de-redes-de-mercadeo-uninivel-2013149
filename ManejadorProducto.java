/**
* manejadores ManejadorProducto acciones de Producto
* @author Pablo Hernandez
*/

package org.pablo.manejadores;

import java.util.ArrayList;
import org.pablo.beans.Producto;


public class ManejadorProducto{
	private ArrayList<Producto> listaProducto;
	private static ManejadorProducto instancia;
	
	private ManejadorProducto(){
		this.listaProducto=new ArrayList<Producto>();
	}
	
	public void agregarProducto(Producto producto){
		this.listaProducto.add(producto);
	}
	public void eliminarProducto(Producto producto){
		this.listaProducto.remove(producto);
	}
	public ArrayList<Producto> obtenerListaProducto(){
		return this.listaProducto;
	}
	public Producto buscarProducto(String id){
		
		for(Producto producto : this.listaProducto){
			if(producto.getId().equals(id))
				return producto;	
		}
		return null;
	}
	public static ManejadorProducto getInstancia(){
		if(instancia==null)
			instancia=new ManejadorProducto();
		return instancia;
	}
	
	public Producto decodifica(String comando){
		String[] comandos=comando.split(",");
		Producto producto = this.buscarProducto(comandos[1]);
		return producto;
	}
	public boolean existe(String id){
		Producto db = this.buscarProducto(id);
		if(db!=null)
			return true;
		return false;
	
	}

}

