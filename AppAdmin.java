/**
* Aplicacion AppAdmin aplicacion de Administrador
* @author Pablo Hernandez
*/


package org.pablo.app;

import org.pablo.utilidades.eventos.DecodeListener;
import org.pablo.utilidades.Decodificador;
import org.pablo.utilidades.Borrar;
import org.pablo.utilidades.Complementos;
import org.pablo.manejadores.ManejadorUsuario;
import org.pablo.manejadores.ManejadorProducto;
import org.pablo.manejadores.ManejadorMiembro;
import org.pablo.manejadores.ManejadorCompra;
import org.pablo.manejadores.ManejadorSQL;
import org.pablo.beans.Compra;
import org.pablo.beans.Miembro;
import org.pablo.beans.Oferta;
import org.pablo.beans.Descuento;
import org.pablo.beans.Usuario;
import org.pablo.beans.Producto;
import org.pablo.sistema.Sistema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;

public class AppAdmin extends AbstractAppRol implements DecodeListener{

	public AppAdmin(Decodificador decodificador){
		decodificador.addDecodeListener(this);	
		super.setDecodificador(decodificador);
		super.setConnected(true);
	}
	
	public void iniciar(){                    
		super.iniciar();
	}
	public void avisarAccionar(String accion, HashMap<String, String> parametros,ArrayList valorUnico){
		Borrar borrar=new Borrar();
		Complementos complementos = new Complementos();
		Usuario autentificado=ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado();
		borrar.limpiar(30);		
		Usuario usuario = new Usuario();
		switch(accion.trim()){
			case "select":
				ManejadorSQL.getInstancia().aQueMeRefiero(parametros.get("from"),valorUnico);
			break;
			case "add user":
				if(parametros.size()>=5){
					if(parametros.get("nombre")!=null)
						usuario.setNombre(parametros.get("nombre"));
					if(parametros.get("apellido")!=null)
						usuario.setApellido(parametros.get("apellido"));
					if(parametros.get("nick")!=null)
						usuario.setNick(parametros.get("nick"));
						if(ManejadorUsuario.getInstancia().existe(parametros.get("nick"))){		
							System.out.println("	El nick  '"+parametros.get("nick")+"' ya esta en uso");
							break;
						}
					if(parametros.get("password")!=null)
						usuario.setPassword(parametros.get("password"));
					if(parametros.get("edad")!=null)
						usuario.setEdad(Integer.parseInt(parametros.get("edad")));					
					usuario.setRol("admin");
					ManejadorUsuario.getInstancia().agregarUsuario(usuario);
					System.out.println("	Usuario agregado ");	
				}else
					System.out.println("	Ingresar nombre,apellido, nick, apellido,password, edad");
				break;			
		case "list downlines":
				
				if(parametros.get("nick")!=null){
					Miembro miembroAMostrar = ManejadorMiembro.getInstancia().buscarUsuario(parametros.get("nick"));
					System.out.println("---------------------------------");
					System.out.println("NOMBRE: "+miembroAMostrar.getNombre());
					System.out.println("");
					this.listarDownlines(miembroAMostrar, 1);
					System.out.println("");
					System.out.println("- - - - - - - - - - - - - - ");
				}else{
					Miembro miembroAMostra= ManejadorMiembro.getInstancia().buscarUsuario("mim");
					System.out.println("---------------------------------");
					System.out.println("NOMBRE: "+miembroAMostra.getNombre());
					System.out.println("");
					this.listarDownlines(miembroAMostra, 1);		
				}
		break;
	
			case "add product":
				
				if(parametros.size()>=2){
					Producto producto = new Producto();
					producto.setNombre(parametros.get("nombre"));
					producto.setPrecio(Integer.parseInt(parametros.get("precio")));
					producto.setId(parametros.get("id"));
					producto.setMarca(parametros.get("marca"));
					ManejadorProducto.getInstancia().agregarProducto(producto);
					System.out.println("	Producto agregado ");	
				}else
					System.out.println("	Ingresar nombre, precio");
				break;						

			case "remove product":
				
				Producto productoAEliminar = ManejadorProducto.getInstancia().buscarProducto(parametros.get("id"));
				if(productoAEliminar!=null){
					ManejadorProducto.getInstancia().eliminarProducto(productoAEliminar);
					System.out.println("	Producto eliminado satisfactoriamente.");
				}else{
					System.out.println(" Necesita parametro id existente");
				}
				break;
			
			case "show me":
				if(codificador(valorUnico,"money")){
					int ganancia=0;
					Miembro miebro;
					for(Miembro mie: ManejadorMiembro.getInstancia().obtenerListaMiembro()){
						ganancia=ganancia+ManejadorUsuario.getInstancia().ganancia(mie.getNivel(),mie.getCompra());
					}
					System.out.println("VOLUMEN MONETARIO "+ganancia);
					
					valorUnico.clear();
				}else{
					borrar.limpiar(2);
					System.out.println("NOMBRE: "+autentificado.getNombre());
					System.out.println("APELLIDO: "+autentificado.getApellido());
					System.out.println("NICK: "+autentificado.getNick());	
					System.out.println("PASSWORD: ***");
					System.out.println("EDAD: "+autentificado.getEdad());
					borrar.limpiar(3);
				}
				break;
			case "edit me":
				if(parametros.size()>=1){
					Usuario usuarioAEditar = ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado();
						if(parametros.get("nombre")!=null)
							usuarioAEditar.setNombre(parametros.get("nombre"));
						if(parametros.get("apellido")!=null)
							usuarioAEditar.setApellido(parametros.get("apellido"));
						if(parametros.get("nick")!=null)
							usuarioAEditar.setNick(parametros.get("nick"));
						if(parametros.get("password")!=null)
							usuarioAEditar.setPassword(parametros.get("password"));
						if(parametros.get("edad")!=null)
							usuarioAEditar.setEdad(Integer.parseInt(parametros.get("edad")));
						System.out.println("	Usuario  modificado ");
				}else
					System.out.println("Ingresar minimo 1 parametro");
				break;
					
			case "edit product":
				String getId=parametros.get("id");
				if(getId!=null){
					Producto productoAEditar = ManejadorProducto.getInstancia().buscarProducto(getId);
					if(ManejadorProducto.getInstancia().existe(getId))
						for(Producto busqueda : ManejadorProducto.getInstancia().obtenerListaProducto()){	
							if(parametros.get("nombre")!=null)
								productoAEditar.setNombre(parametros.get("nombre"));
							if(parametros.get("precio")!=null)
								productoAEditar.setPrecio(Integer.parseInt(parametros.get("precio")));
							if(parametros.get("marca")!=null)
								productoAEditar.setMarca(parametros.get("marca"));
								System.out.println("	*Producto modificado ");
						}
				}else
					System.out.println("	Ingresar id del producto");
				break;	
			case "show sales":
				if(parametros.get("nombre")!=null){
					for(Compra producto:ManejadorCompra.getInstancia().obtenerListaCompra()){
						if(producto.getProducto().equals(parametros.get("nombre"))){
						System.out.println("COMPRADO POR "+producto.getMiembro());
						System.out.println("FECHA "+producto.getDate());
						System.out.println("- - - - - - - - - - - - - -  ");
						System.out.println(" ");
						}
					}
				}else{
					for(Compra producto:ManejadorCompra.getInstancia().obtenerListaCompra()){
						System.out.println("PRODUCTO "+producto.getProducto());
						System.out.println("COMPRADO POR "+producto.getMiembro());
						System.out.println("FECHA "+producto.getDate());
						System.out.println("- - - - - - - - - - - - - -  ");
						System.out.println(" ");
				
					}
				}
				break;
			case "add ofert":
			
				if(parametros.get("idproduct")!=null&parametros.get("cantidad")!=null&parametros.get("preciode")!=null){
					if(ManejadorProducto.getInstancia().existe(parametros.get("idproduct"))){
						int cantidad=Integer.parseInt(parametros.get("cantidad"));
						int precioDe=Integer.parseInt(parametros.get("preciode"));
						Producto ofertado=ManejadorProducto.getInstancia().buscarProducto(parametros.get("idproduct"));
						int total=(cantidad*ofertado.getPrecio());
						int ahorro=(total-(precioDe*ofertado.getPrecio()));
						Oferta oferta=new Oferta();
						oferta.setCantidadDeProducto(cantidad);
						oferta.setCantidadDePrecio(precioDe);
						oferta.setTotal(total);
						oferta.setAhorro(ahorro);
						oferta.setId("ofert,"+ofertado.getId());
						ofertado.setOferta(oferta);
						System.out.println("	*Oferta agredada");
						}
					}else if(parametros.get("idproduct")!=null&parametros.get("porcentaje")!=null){
						if(ManejadorProducto.getInstancia().existe(parametros.get("idproduct"))){
							Producto ofertado=ManejadorProducto.getInstancia().buscarProducto(parametros.get("idproduct"));
							int porcentaje=Integer.parseInt(parametros.get("porcentaje"));
							int tota=ofertado.getPrecio();
							int ahorr=((ofertado.getPrecio()*porcentaje)/100);	
							Descuento descuento=new Descuento();
							descuento.setId("desc,"+ofertado.getId());
							descuento.setPorcentaje(porcentaje);
							descuento.setTotal(tota);
							descuento.setAhorro(ahorr);
							ofertado.setDescuento(descuento);
							System.out.println("	*Oferta agredada");
							}
					}
			break;
			default:
				super.revisarAccionar(accion, parametros,valorUnico);
		}	
	}
}