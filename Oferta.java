package org.pablo.beans;
import java.util.ArrayList;
public class Oferta{

	private int  cantidadDeProducto;
	private  int cantidadDePrecio;
	private int total;
	private int ahorro;
	private String id;
	public Oferta(){}
	private ArrayList<Oferta> propiedadOferta;
	
	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id=id;
	}
	public int getCantidadDeProducto(){
		return this.cantidadDeProducto;
	}
	public void setCantidadDeProducto(int cantidadDeProducto){
		this.cantidadDeProducto=cantidadDeProducto;
	}
	public int getCantidadDePrecio(){
		return this.cantidadDePrecio;
	}
	public void setCantidadDePrecio(int cantidadDePrecio){
		this.cantidadDePrecio=cantidadDePrecio;
	}
	public int getTotal(){
		return this.total;
	}
	public void setTotal(int total){
		this.total=total;
	}
	public void setAhorro(int ahorro){
		this.ahorro=ahorro;
	}
	public int getAhorro(){
		return this.ahorro;
	}
	public ArrayList<Oferta> getListaOferta(){
		return this.propiedadOferta;
	}
	public void setListaOferta(ArrayList<Oferta>propiedadOferta){
		this.propiedadOferta=propiedadOferta;
	}
	public Oferta(String id,int cantidadDeProducto,int cantidadDePrecio,int total,int ahorro){
		this.setId(id);
		this.setCantidadDeProducto(cantidadDeProducto);
		this.setCantidadDePrecio(cantidadDePrecio);
		this.setTotal(total);
		this.setAhorro(ahorro);
	}
}