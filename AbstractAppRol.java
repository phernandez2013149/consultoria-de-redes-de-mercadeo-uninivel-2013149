/**
* Aplicacion AbstractAppRol Aplicacion abstracta 
* @author Pablo Hernandez
*/

package org.pablo.app;

import java.util.HashMap;
import java.util.ArrayList;

import org.pablo.utilidades.Lee;
import org.pablo.utilidades.Decodificador;
import org.pablo.utilidades.Borrar;
import org.pablo.utilidades.Complementos;
import org.pablo.beans.Miembro;
import org.pablo.beans.Producto;
import org.pablo.beans.ListaInterna;
import org.pablo.beans.Compra;
import org.pablo.beans.Oferta;
import org.pablo.beans.Descuento;
import org.pablo.manejadores.ManejadorCompra;
import org.pablo.manejadores.ManejadorMiembro;
import org.pablo.manejadores.ManejadorProducto;


public abstract class AbstractAppRol{
	
	Complementos complementos= new Complementos();
	Borrar borrar= new Borrar();
	private ArrayList<Miembro>buscarPorParametro=new ArrayList<Miembro>();
	private ArrayList<Miembro> nickMiembro;
	private ArrayList<String> dentro= new ArrayList<String>();
	private Decodificador decodificador;
	private Miembro globo;
	private boolean estadoSesion;
	private String nick ;
	private Miembro este;
		
	public Decodificador getDecodificador(){
		return this.decodificador;
	}
	public void setDecodificador(Decodificador decodificador){
		this.decodificador=decodificador;
	}
	public boolean isConnected(){
		return this.estadoSesion;
	}
	public void setConnected(boolean estadoSesion){
		this.estadoSesion=estadoSesion;
	}
	public boolean codificador(ArrayList valorUnico,String parametro){
		
		for(int contador=0;contador<valorUnico.size();contador++){
			if(valorUnico.get(contador).equals(parametro))
			return true;
		}
		return false;
	}
	
	public String obtenerLineas(int nivel){
		String linea="";
		for(int contadorNivel=0;contadorNivel<nivel;contadorNivel++){
			linea+="	";
		}
		return linea;
	}
	
	public void listarDownlines(Miembro miembro, int nivel){
		String numeroDeLineas=this.obtenerLineas(nivel);
		for(Miembro mim : miembro.getListaMiembro()){
	
			System.out.println(numeroDeLineas+"|NOMBRE: "+mim.getNombre());
			System.out.println(numeroDeLineas+"|APELLIDO: "+mim.getApellido());
			System.out.println(numeroDeLineas+"|NICK: "+mim.getNick());
			System.out.println(numeroDeLineas+"|UPLINE: "+mim.getUpLine());
			System.out.println(numeroDeLineas+"|NIVEL: "+mim.getNivel());
			System.out.println(numeroDeLineas+"|INCRITO POR: "+mim.getInscriptor());
			System.out.println(numeroDeLineas+"|MIEMBRO DESDE: "+mim.getFecha());
			System.out.println(numeroDeLineas+"-----------------------");
			this.listarDownlines(mim, nivel+1);
		}
	}
	public ArrayList<Miembro> listaDeBuscadosPorParametro(String parametro){
		
		for(Miembro buscarPor : ManejadorMiembro.getInstancia().obtenerListaMiembro()){
			if(parametro.equals(buscarPor.getNombre()))
				buscarPorParametro.add(buscarPor);
			else if(parametro.equals(buscarPor.getNick()))
				buscarPorParametro.add(buscarPor);
			else if(parametro.equals(buscarPor.getApellido()))
				buscarPorParametro.add(buscarPor);
			else if(parametro.equals(buscarPor.getUpLine()))
				buscarPorParametro.add(buscarPor);
			else if(parametro.equals(buscarPor.getEdad()))
				buscarPorParametro.add(buscarPor);
		}
		return  this.buscarPorParametro;
		}
	
	public void revisarAccionar(String accion, HashMap<String,String> parametros,ArrayList valorUnico){
		Miembro autentificado = ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado();
		borrar.limpiar(30);
		switch(accion.trim()){
			

		
			case "search downline":
			System.out.println("	|Busqueda ");
				if(parametros.get("dato")!=null){	
					for(Miembro lista : this.listaDeBuscadosPorParametro(parametros.get("dato"))){
						if(buscarPorParametro!=null){
						System.out.println("NOMBRE: "+lista.getNombre());
						System.out.println("APELLIDO: "+lista.getApellido());
						System.out.println("NICK: "+lista.getNick());
						System.out.println("PASSWORD: ***** ");
						System.out.println("EDAD: "+lista.getEdad());
						System.out.println("INSCRIPO POR: "+lista.getUpLine());
						System.out.println("MIEMBRO DESDE: "+lista.getFecha());
						System.out.println("- - - - - - - - - - - - - - ");
						System.out.println(" ");
						}else
						System.out.println("	No hay ningun miembro que coicida con la busqueda");
					}
				buscarPorParametro.clear();
					
				}else
				System.out.println("Necesita parametro dato");
			break;	
			case "list ofert":
				for(Oferta of :ManejadorCompra.getInstancia().obtenerListaOferta()){
					System.out.println("- - - - - - - - - - - - - ");
					System.out.println("id"+of.getId());
					System.out.println("ahorro"+of.getAhorro());
				}
			break;
			case "list products":
				
					for(Producto pro : ManejadorProducto.getInstancia().obtenerListaProducto()){
						if(pro.getOferta()!=null){
							System.out.println("	*EN OFERTA!");
							Producto producto=ManejadorProducto.getInstancia().buscarProducto(pro.getId());
							Oferta oferta=producto.getOferta();
							System.out.println("NOMBRE: "+pro.getNombre());
							System.out.println("PRECIO: "+pro.getPrecio());
							System.out.println("IDOFERTA: "+oferta.getId());
							System.out.println("OFERTA: "+oferta.getCantidadDeProducto()+"X"+oferta.getCantidadDePrecio());
							System.out.println("AHORRO DE: "+oferta.getAhorro());
							System.out.println("- - - - - - - - - - - - - ");
						}else if(pro.getDescuento()!=null){
							Descuento descuento = pro.getDescuento();
							System.out.println("NOMBRE: "+pro.getNombre());
							System.out.println("PRECIO: "+pro.getPrecio());
							System.out.println("IDOFERTA: "+descuento.getId());
							System.out.println("DESCUENTO: "+descuento.getPorcentaje()+"%");
							System.out.println("AHORRO DE: "+descuento.getAhorro());
							System.out.println("- - - - - - - - - - - - - ");
						
						}
					}
						System.out.println("");
						System.out.println("- - - - - - - - - - - - - ");
					for(Producto pro : ManejadorProducto.getInstancia().obtenerListaProducto()){
						System.out.println("NOMBRE: "+pro.getNombre());
						System.out.println("PRECIO: "+pro.getPrecio());
						System.out.println("- - - - - - - - - - - - - ");
					}
					

				break;
			case "show product":
				if(parametros.get("nombre")!=null)
					for(Producto produ : ManejadorProducto.getInstancia().obtenerListaProducto()){
						if(produ.getNombre().equals(parametros.get("nombre"))){
						System.out.println("NOMBRE: "+produ.getNombre());
						System.out.println("PRECIO: "+produ.getPrecio());
						System.out.println("ID: "+produ.getId());
						System.out.println("MARCA: "+produ.getMarca());

						borrar.limpiar(2);
						}
					}
				else
				System.out.println("Necesita parametro nombre");
				break;
				
		
			case "show downline":
				if(parametros.get("nick")!=null){
					if(this.codificador(valorUnico,"money")){
						valorUnico.clear();
					}else{
						Miembro buscado = ManejadorMiembro.getInstancia().buscarUsuario(parametros.get("nick"));
						if(buscado!=null){
							System.out.println("NOMBRE: "+buscado.getNombre());
							System.out.println("APELLIDO: "+buscado.getApellido());
							System.out.println("NICK: "+buscado.getNick());
							System.out.println("PASSWORD: ***** ");
							System.out.println("EDAD: "+buscado.getEdad());
							System.out.println("NIVEL: "+buscado.getNivel());
							System.out.println("UPLINE: "+buscado.getUpLine());
							System.out.println("INSCRIPO POR: "+buscado.getInscriptor());
						}else
							System.out.println("El usuario no existe");
					}
				}else
				System.out.println("Necesita parametro nick");
			break;
			case "show history":
			
				if(codificador(valorUnico,"buy")){
					
						if(codificador(valorUnico,"downline")&codificador(valorUnico,"buy")&parametros.get("nick")!=null){
							Miembro miembroAMostrar = ManejadorMiembro.getInstancia().buscarUsuario(parametros.get("nick"));
							for(Compra producto:ManejadorCompra.getInstancia().obtenerListaCompra()){
								if(producto.getMiembro().equals(miembroAMostrar.getNick())){
									System.out.println("PRODUCTO "+producto.getProducto());
									System.out.println("FECHA "+producto.getDate());
									System.out.println("- - - - - - - - - - - - - -  ");
									System.out.println(" ");
				
								System.out.println("");
								System.out.println("- - - - - - - - - - - - - - ");}
							}
						}else if(codificador(valorUnico,"buy")){
							for(Compra producto:ManejadorCompra.getInstancia().obtenerListaCompra()){
								if(producto.getMiembro().equals(autentificado.getNick())){
									System.out.println("PRODUCTO "+producto.getProducto());
									System.out.println("FECHA "+producto.getDate());
									System.out.println("- - - - - - - - - - - - - -  ");
									System.out.println(" ");
						}
					}
				
						}

				}else if(codificador(valorUnico,"downline")){			
					if(parametros.get("nick")!=null){
						Miembro miembroAMostrar = ManejadorMiembro.getInstancia().buscarUsuario(parametros.get("nick"));
						System.out.println("---------------------------------");
						System.out.println("NOMBRE: "+miembroAMostrar.getNombre());
						System.out.println("");
						this.listarDownlines(miembroAMostrar, 1);
				
						System.out.println("");
						System.out.println("- - - - - - - - - - - - - - ");
						break;
				
					}else{
					
						if(autentificado!=null){
						System.out.println("---------------------------------");
						System.out.println("NOMBRE: "+autentificado.getNombre());
						System.out.println("");
						this.listarDownlines(autentificado, 1);
							break;
						}else{
						Miembro miembroAMostra = ManejadorMiembro.getInstancia().buscarUsuario("mim");
						System.out.println("---------------------------------");
						System.out.println("NOMBRE: "+miembroAMostra.getNombre());
						System.out.println("");
						this.listarDownlines(miembroAMostra, 1);
						break;
						}
					}
				}else{
					if(parametros.get("nick")!=null){
						Miembro miembroAMostrar = ManejadorMiembro.getInstancia().buscarUsuario(parametros.get("nick"));
						System.out.println("---------------------------------");
						System.out.println("NOMBRE: "+miembroAMostrar.getNombre());
						System.out.println("");
						this.listarDownlines(miembroAMostrar, 1);
				
						System.out.println("");
						System.out.println("- - - - - - - - - - - - - - ");
						break;
				
					}else{
					
						if(autentificado!=null){
						System.out.println("---------------------------------");
						System.out.println("NOMBRE: "+autentificado.getNombre());
						System.out.println("");
						this.listarDownlines(autentificado, 1);
							break;
						}else{
						Miembro miembroAMostra = ManejadorMiembro.getInstancia().buscarUsuario("mim");
						System.out.println("---------------------------------");
						System.out.println("NOMBRE: "+miembroAMostra.getNombre());
						System.out.println("");
						this.listarDownlines(miembroAMostra, 1);
						break;
					
						}
					}
				}
			valorUnico.clear();
			break;
	
			case "cls":
			break;
			
			case "logout":
				setConnected(false);
				autentificado=null;
			break;
			case "help":
				complementos.helpMiembro();
			break;
			
			default:
				complementos.referencia(accion);	
		}
	}
	
	public void iniciar(){
		Lee leer = new Lee();
		do{
			System.out.print(">> ");
			String comando=leer.dato();
			if(this.decodificador!=null){
				this.decodificador.decodificarComando(comando);
			}
		}while(this.isConnected());
	}
}