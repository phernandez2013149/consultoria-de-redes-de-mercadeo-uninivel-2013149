/**
* bean Producto aplicando encapsulamiento 
* @author Pablo Hernandez
*/

package org.pablo.beans;
import org.pablo.beans.Oferta;

public class Producto{

	private String nombre;
	private String id;
	private String marca;
	private int precio;
	private Oferta oferta;
	private Descuento descuento;
	public Producto(){}
	
	public Producto(String nombre,int precio,String id,String marca,Oferta oferta,Descuento descuento){
		this.setNombre(nombre);
		this.setPrecio(precio);
		this.setId(id);
		this.setMarca(marca);
		this.setOferta(oferta);
		this.setDescuento(descuento);
	}
	
	public String getNombre(){
		return this.nombre;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public int getPrecio(){
		return this.precio;
	}
	public void setPrecio(int precio){
		this.precio=precio;
	}
	public void setId(String id){
		this.id=id;
	}
	public String getId(){
		return this.id;
	}
	public Oferta getOferta(){
		return this.oferta;
	}
	public void setOferta(Oferta oferta){
		this.oferta=oferta;
	}
	public Descuento getDescuento(){
		return this.descuento;
	}
	public void setDescuento(Descuento descuento){
		this.descuento=descuento;
	}
	public void setMarca(String marca){
		this.marca=marca;
	}
	public String getMarca(){
		return this.marca;
	}
}