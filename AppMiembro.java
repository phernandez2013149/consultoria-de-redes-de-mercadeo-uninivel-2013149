/**
* Aplicacion AppMiemmbro  
* @author Pablo Hernandez
*/

package org.pablo.app;

import org.pablo.utilidades.eventos.DecodeListener;
import org.pablo.utilidades.Lee;
import org.pablo.utilidades.Decodificador;
import org.pablo.utilidades.Borrar;
import org.pablo.utilidades.Complementos;
import org.pablo.manejadores.ManejadorMiembro;
import org.pablo.manejadores.ManejadorCompra;
import org.pablo.manejadores.ManejadorProducto;
import org.pablo.beans.Producto;
import org.pablo.beans.Miembro;
import org.pablo.beans.Compra;
import org.pablo.beans.Oferta;
import org.pablo.beans.Descuento;
import org.pablo.beans.ListaInterna;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;

public class AppMiembro extends AbstractAppRol implements DecodeListener{

	public AppMiembro(Decodificador decodificador){
		
		decodificador.addDecodeListener(this);	
		super.setDecodificador(decodificador);
		super.setConnected(true);
	}
	
	public void iniciar(){                    
		
		super.iniciar();
	}
	public String obtenerLineas(int nivel){
		String resultado="";
			for(int posicion=0;posicion<nivel;posicion++){
					resultado+="     ";
			}
		return resultado;
	}
	
		/** @param accion, parametros,valorUnico pedimos valor para realizar acciones*/
	public void avisarAccionar(String accion, HashMap<String, String> parametros,ArrayList valorUnico){
		ArrayList<Miembro> misRed;
		Borrar borrar=new Borrar();
		Complementos complementos = new Complementos();
		Miembro autentificado ;
		Miembro miembro = new Miembro();
		Compra compra;
		Miembro up;
		borrar.limpiar(30);
		//boolean codificador;
		switch(accion.trim()){
	
			case "add downline":
				autentificado = ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado();
				if(parametros.size()>=3){
						if(parametros.get("nombre")!=null)
							miembro.setNombre(parametros.get("nombre"));
						if(parametros.get("apellido")!=null)
							miembro.setApellido(parametros.get("apellido"));						
						
						if(parametros.get("nick")!=null){
							miembro.setNick(parametros.get("nick"));
							if(ManejadorMiembro.getInstancia().existe(parametros.get("nick"))){
								System.out.println("	El nick "+parametros.get("nick")+" ya esta en uso");
								break;
							}
						}
						if(parametros.get("producto")!=null){
							compra=new Compra();
							Producto proBuscado= ManejadorProducto.getInstancia().buscarProducto(parametros.get("product"));
							if(proBuscado!=null){
								miembro.setCompra(miembro.getCompra()+proBuscado.getPrecio());
								compra.setMiembro(miembro.getNick());
								compra.setProducto(parametros.get("product"));
								compra.setDate(new Date());
								ManejadorCompra.getInstancia().agregaCompra(compra);	
							}
						}
						if(parametros.get("password")!=null)
							miembro.setPassword(parametros.get("password"));
						if(parametros.get("edad")!=null)
							
							miembro.setEdad(Integer.parseInt(parametros.get("edad")));
						if(parametros.get("pin")!=null)
							miembro.setEdad(Integer.parseInt(parametros.get("pin")));
							
							miembro.setRol("miembro");
							miembro.setInscriptor(autentificado.getNick());
							miembro.setFecha(new Date());
	
						if(parametros.get("down")!=null){
							String[] ruta = parametros.get("down").split(",");
							if(ManejadorMiembro.getInstancia().existe(ruta[0])){
							Miembro miembroR = ManejadorMiembro.getInstancia().buscarUsuario(ruta[0]);
							Miembro miembroResultado = miembroR;
								for(int posicion=1;posicion<ruta.length;posicion++){
									miembroResultado=ManejadorMiembro.getInstancia().buscarDentroDeMiembro(miembroR, ruta[posicion]);
								}
							miembro.setUpLine(parametros.get("down"));
							up =ManejadorMiembro.getInstancia().buscarUsuario(miembro.getUpLine());
							miembro.setNivel(up.getNivel()+1);
							miembroResultado.getListaMiembro().add(miembro);	
							ManejadorMiembro.getInstancia().agregarUsuario(miembro);
							System.out.println("	Usuario  agregado ");
							}else
							System.out.println("El upline no existe");
						}else{
							String ruta=autentificado.getNick();
							Miembro miembroR = ManejadorMiembro.getInstancia().buscarUsuario(ruta);
							Miembro miembroResultado = miembroR;
							ManejadorMiembro.getInstancia().buscarDentroDeMiembro(miembroR, ruta);
							miembro.setUpLine(ruta);
							up =ManejadorMiembro.getInstancia().buscarUsuario(miembro.getUpLine());
							miembro.setNivel(up.getNivel()+1);

							miembroResultado.getListaMiembro().add(miembro);	
							ManejadorMiembro.getInstancia().agregarUsuario(miembro);
							System.out.println("	Usuario  agregado ");
						}

				}else	
					System.out.println("Debe de ingresar: nombre-apellido-nick-password-edad-pin");
		break;
		
		case "buy product":				
		//buy product id=idProduc [pin=0000]
		autentificado=ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado();
			compra=new Compra();
			
			if(parametros.get("id")!=null){
				Producto este=null;
				try{
					este=ManejadorProducto.getInstancia().decodifica(parametros.get("id"));
				}catch(ArrayIndexOutOfBoundsException err){}
				
				if(ManejadorProducto.getInstancia().existe(parametros.get("id"))){
				
					Producto proBuscado= ManejadorProducto.getInstancia().buscarProducto(parametros.get("id"));
					autentificado.setCompra(autentificado.getCompra()+proBuscado.getPrecio());
					compra.setProducto(parametros.get("id"));
					compra.setDate(new Date());
					compra.setMiembro(autentificado.getNick());
					ManejadorCompra.getInstancia().agregaCompra(compra);
					System.out.println("	Agregado al carrito! ");
				}else{		
					if(este.getDescuento()!=null){
						Descuento descuento=este.getDescuento();
						autentificado.setCompra(autentificado.getCompra()+descuento.getTotal());
					}else{
						Oferta oferta =este.getOferta();
						autentificado.setCompra(autentificado.getCompra()+oferta.getTotal());
					}
						compra.setProducto(parametros.get("id"));
						compra.setDate(new Date());
						compra.setMiembro(autentificado.getNick());
						ManejadorCompra.getInstancia().agregaCompra(compra);
						System.out.println("	Agregado al carrito! ");
				}
				
			}else{
				System.out.println("Debe de ingresar id del producto, [pin]");
			}
				break;	
			
			case "list downlines":
				autentificado = ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado();
				if(parametros.get("nick")!=null){
					Miembro miembroAMostrar = ManejadorMiembro.getInstancia().buscarUsuario(parametros.get("nick"));
					System.out.println("---------------------------------");
					System.out.println("NOMBRE: "+miembroAMostrar.getNombre());
					System.out.println("");
					this.listarDownlines(miembroAMostrar, 1);		
					System.out.println("");
					System.out.println("- - - - - - - - - - - - - - ");
				}else {
					System.out.println("---------------------------------");
					System.out.println("NOMBRE: "+autentificado.getNombre());
					System.out.println("");
					this.listarDownlines(autentificado, 1);
				}
				break;
				
			case "edit me":
					if(parametros.size()>=1){;
						Miembro miembroEditar=ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado();
						if(parametros.get("nombre")!=null)
							miembroEditar.setNombre(parametros.get("nombre"));
						if(parametros.get("apellido")!=null)
							miembroEditar.setApellido(parametros.get("apellido"));
						if(parametros.get("nick")!=null)
							miembroEditar.setNick(parametros.get("nick"));
						if(parametros.get("password")!=null)
							miembroEditar.setPassword(parametros.get("password"));
						if(parametros.get("edad")!=null)
							miembroEditar.setEdad(Integer.parseInt(parametros.get("edad")));
						if(parametros.get("pin")!=null)
							miembroEditar.setPin(Integer.parseInt(parametros.get("pin")));
					}
						System.out.println("	Usuario  modificado ");
				break;
			
			case "show me":
			
			autentificado=ManejadorMiembro.getInstancia().obtenerUsuarioAutenticado();
				if(codificador(valorUnico,"money")){
					int nivelA;
					int ganancia=0;
					for(Miembro mie: autentificado.getListaMiembro()){
						nivelA=mie.getNivel()-autentificado.getNivel();
						ganancia=ganancia+ManejadorMiembro.getInstancia().ganancia(nivelA,mie.getCompra());
					}
					System.out.println("VOLUMEN MONETARIO "+ganancia);
					valorUnico.clear();
				}else{
					System.out.println("NOMBRE: "+autentificado.getNombre());
					System.out.println("APELLIDO: "+autentificado.getApellido());
					System.out.println("NICK: "+autentificado.getNick());	
					System.out.println("PASSWORD: ***");
					System.out.println("EDAD: "+autentificado.getEdad());
					System.out.println("NIVEL: "+autentificado.getNivel());
					System.out.println("UPLINE: "+autentificado.getUpLine());
					System.out.println("INVERSIONES: "+autentificado.getCompra());
					System.out.println("INSCRITO POR: "+autentificado.getInscriptor());
					System.out.println("MIEMBRO DESDE: "+autentificado.getFecha());
				}
				valorUnico.clear();
				break;
		
			case "logout":
				super.setConnected(false);
				break;
			case "help":
				complementos.helpMiembro();
				break;
			default:
				super.revisarAccionar(accion, parametros,valorUnico);
		}
	}
}


